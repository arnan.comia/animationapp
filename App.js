import React from 'react';
import { View } from 'react-native';
import AppNavigator from './src/components/AppNavigator';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <AppNavigator />
      </View>
    )
  }
}
const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alingItems: 'center',
    justifyContent: 'center'
  }
}

console.disableYellowBox = true;