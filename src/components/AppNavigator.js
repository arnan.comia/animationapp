import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Dashboard from './Dashboard';
import ScollAnimation from './ScrollAnimation';

const AppNavigator = createStackNavigator({
  Dashboard: { screen: Dashboard,
    navigationOptions:{
      header: null
  }},
  ScollAnimation: { screen: ScollAnimation
  },
})

export default createAppContainer(AppNavigator);
