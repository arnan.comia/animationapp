import React from 'react';
import {
  Animated,
  FlatList,
  View,
  Dimensions
} from 'react-native';
import { Icon } from 'react-native-elements';

const row = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
const { height, width } = Dimensions.get('screen')

export default class ScrollAnimation extends React.Component {
    state = {
      shouldShowStickyHeader: false,
      scrollY: new Animated.Value(1),
      animatedPadding: 0
    }
  
    componentDidMount() {
      this.state.scrollY.addListener(({ value }) => {
        const shouldShowStickyHeader = value > 90
        this.setState({ shouldShowStickyHeader, animatedPadding: value })
      })
    }
  
    keyExtractor = item => item.toString()
  
    renderItem = item => {
      const { evenRowStyle, oddRowStyle } = styles
      if (item % 2 === 0) return <View style={evenRowStyle} />
      return <View style={oddRowStyle} />
    }
  
    renderHeader = () => {
      const { animatedViewStyle } = styles
      let { shouldShowStickyHeader, animatedPadding } = this.state
  
      if (animatedPadding > 90) animatedPadding = 90 
      const animatedStyle = {
        paddingLeft: animatedPadding,
        paddingRight: animatedPadding / 2
      }
      if (!shouldShowStickyHeader) {
        return (
          <Animated.View style={[animatedViewStyle, animatedStyle]}>
            <Icon
              reverse
              name='ios-american-football'
              type='ionicon'
              color='#517fa4'
            />
            <Icon
              reverse
              name='ios-heart'
              type='ionicon'
              color='#f50'
            />
            <Icon
              reverse
              name='ios-american-football'
              type='ionicon'
              color='#517fa4'
            />
            <Icon
              reverse
              name='ios-heart'
              type='ionicon'
              color='#f50'
            />
          </Animated.View>
        )
      }
      return <View />
    }
  
    renderStickerHeader = () => {
      const { stickerHeaderStyle } = styles
      return (
        <View
          style={stickerHeaderStyle}>
          <Icon
            name='ios-american-football'
            type='ionicon'
            color='#517fa4'
          />
          <Icon
            name='ios-heart'
            type='ionicon'
            color='#f50'
          />
          <Icon
            name='ios-american-football'
            type='ionicon'
            color='#517fa4'
          />
          <Icon
            name='ios-heart'
            type='ionicon'
            color='#f50'
          />
        </View>
      )
    }
    render() {
      return (
        <View style={styles.container}>
          {this.state.shouldShowStickyHeader && this.renderStickerHeader()}
          <FlatList
            data={row}
            keyExtractor={this.keyExtractor}
            ListHeaderComponent={this.renderHeader()}
            renderItem={({ item }) => this.renderItem(item)}
            onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }])}
          >
  
          </FlatList>
        </View>
      )
    }
  }
  
  const styles = {
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alingItems: 'center',
      justifyContent: 'center'
    },
    evenRowStyle: {
      width,
      backgroundColor: 'lightblue',
      height: height * 0.2
    },
    oddRowStyle: {
      width,
      backgroundColor: 'lightpink',
      height: height * 0.2
    },
    stickerHeaderStyle: {
      width,
      height: 90,
      paddingLeft: 80,
      paddingRight: 40,
      paddingBottom: 20,
      flexDirection: 'row',
      alignItems: 'flex-end',
      justifyContent: 'space-around',
      backgroundColor: 'yellow'
    },
    animatedViewStyle: {
      width,
      height: 180,
      flexDirection: 'row',
      alignItems: 'flex-end',
      backgroundColor: 'yellow',
      justifyContent: 'space-around',
    }
  }
  
  