import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'native-base'

export default class Dashboard extends React.Component {
    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <Button 
                    onPress={() => navigate('ScrollAnimation')}>
                    <Text>Scroll Animation</Text>
                </Button>
            </View>
        )
    }
}
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alingItems: 'center',
        justifyContent: 'center'
    }
}
